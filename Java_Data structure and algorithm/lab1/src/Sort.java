package lab1;

public class Sort {
    //static XYPoint[] local=new XYPoint[10000];
    //int l=0;
    //================================= sort =================================
    //
    // Input: array A of XYPoints refs 
    //        lessThan is the function used to compare points
    //
    // Output: Upon completion, array A is sorted in nondecreasing order
    //         by lessThan.
    //
    // DO NOT USE ARRAYS.SORT.  WE WILL CHECK FOR THIS.  YOU WILL GET A 0.
    // I HAVE GREP AND I AM NOT AFRAID TO USE IT.
    //=========================================================================
    public static void msort(XYPoint[] A, Comparator lessThan) {
	   //Implement your sort here.  (use mergeSort).
    	int l=A.length;
    	if (l==0) {
    		return;
    	}
    	if (l==1) {
    		return;
    	}
    	mergesort(A,0,A.length-1,lessThan);
    /*	for (int i=0;i<A.length;i++){
    		System.out.println(A[i]+"	"+local[i]);
    	}*/
    	
    }
    
    static void mergesort(XYPoint[] A,int l,int r, Comparator lessThan){
    	if (l<r) {
    		int mid= (l+r)/2;
    		mergesort(A,l,mid,lessThan);
    		mergesort(A,mid+1,r,lessThan);
    		merge(A,l,mid,r,lessThan);
    	}
    }
    
    static void merge(XYPoint[] A,int l,int mid,int r, Comparator lessThan){
    	int i,j,k;
        XYPoint[] local=new XYPoint[r-l+1];
    	i=l;j=mid+1;k=0;
    	while ((i<=mid)&&(j<=r)) {
    		if (lessThan.comp(A[i],A[j])) {
    			local[k]=A[i];
    			k++;
    			i++;
    		}else{
    			local[k]=A[j];
    			k++;
    			j++;
    		}
    	}
    	while (i<=mid) {
    		local[k]=A[i];
    		k++;
    		i++;
    	}
    	while (j<=r) {
    		local[k]=A[j];
    		k++;
    		j++;
    	}
    	for (int ii=0;ii<k;ii++){
    		A[ii+l]=local[ii];
    	}
    }
    
}
