package lab1;

public class ClosestPair {
	static XYPoint cl1= new XYPoint();
	static XYPoint cl2= new XYPoint();
    public final static double INF = java.lang.Double.POSITIVE_INFINITY;
	static double cld=INF;
    /** 
     * Given a collection of points, find the closest pair of point and the
     * distance between them in the form "(x1, y1) (x2, y2) distance"
     *
     * @param pointsByX points sorted in nondecreasing order by X coordinate
     * @param pointsByY points sorted in nondecreasing order by Y coordinate
     * @return Result object containing the points and distance
     */

    static Result findClosestPair(XYPoint pointsByX[], XYPoint pointsByY[]) {
    	int leng=pointsByX.length;
    	XYPoint nn=new XYPoint();
    	if (leng==0) {
    		Result rMin0 = new Result(nn, nn, 0);
    		return rMin0;
    	}
    	if (leng==1){
    		Result rMin1 = new Result(pointsByX[0], pointsByX[0], 0);
    		return rMin1;
    	}
       // YOUR CODE GOES HERE
        cld=INF;
        findcl(pointsByX,0,pointsByX.length-1);
        if((cl1.x==cl2.x)&&(cl1.y>cl2.y)){
		  XYPoint te=cl1;
		  cl1=cl2;
		  cl2=te;
        }

    	Result rMin = new Result(cl1, cl2, cld);
    	return rMin;
    }
    
    static int abs(int x) {
        if (x < 0) {
            return -x;
        } else {
            return x;
        }
    }
    
    static void findcl(XYPoint[] pointsByX,int l,int r){
	if (l==(r-1)){
		if  ((pointsByX[l].dist(pointsByX[r])<cld)||
                (
                    (pointsByX[l].dist(pointsByX[r])==cld)&&
                        (
                            (pointsByX[l].x<cl1.x)||( (pointsByX[l].x==cl1.x)&&(pointsByX[l].y<cl1.y) )
                    )
                )
            ) {
    	    		cld=pointsByX[l].dist(pointsByX[r]);
    	    		cl1=pointsByX[l];
    	    		cl2=pointsByX[r];
    	    	}
	}
    	if (l<(r-1)) {
    		int mid = (r+l)/2;
    		findcl(pointsByX,l,mid);
    		findcl(pointsByX,mid+1,r);
    		merge(pointsByX,l,mid,r);
    	}
    }
    
    static void merge(XYPoint[] pointsByX,int l,int mid,int r){
    	int line1=mid;
    	int line2=mid+1;
    	while (line1>l){
    		if (abs(pointsByX[line1].x-pointsByX[mid].x)<=cld){
    			line1--;}else{
                    break;
                }
    			
    	}
    	while (line2<r){
    		//line2++;
    		if (abs(pointsByX[line2].x-pointsByX[mid].x)<=cld){
    			line2++;}
                else{
                    //line2--;
                    break;
                }
    	}
    	if (line1!=line2){
    		for (int i=line1;i<=mid;i++){
    			for (int j=mid+1;j<=line2;j++){
    				if ((pointsByX[i].dist(pointsByX[j])<cld)||
                        ((pointsByX[i].dist(pointsByX[j])==cld)&&
                    ((pointsByX[i].x<cl1.x)
                    ||((pointsByX[i].x==cl1.x)&&(pointsByX[i].y<cl1.y))
                    ))) {
    	    				cld=pointsByX[i].dist(pointsByX[j]);
    	    				cl1=pointsByX[i];
    	    				cl2=pointsByX[j];
    	    			}
    			}
    		}
    	}
    }
}
