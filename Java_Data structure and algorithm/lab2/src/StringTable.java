package lab2;

/**
 * A hash table mapping Strings to their positions in the pattern sequence.
 *
 * Fill in the methods for this class.
 */
public class StringTable {
	private static int length;
	private static Record[] table;
	private static int eNum;
    private final static Record rmvd=new Record("d");
    private static int length2;
    //private static boolean trydb=false;
    /**
     * Create an empty table of size n
     *
     * @param n size of the table
     */
    public StringTable(int n) {
        // TODO: implement this method
    	length2=n;

        int m=1;
        while (m<n) {
            m=m*2;
        }
        length=m;
        table=new Record[length];
        eNum=0;
    }



    /**
     * Create an empty table.  You should use this construction when you are
     * implementing the doubling procedure.
     */
    public StringTable() {
        // TODO: implement this method
    	length=4;
        length2=4;
    	table=new Record[length];
    	eNum=0;
    }

    /**
     * Insert a Record r into the table.
     *
     * If you get two insertions with the same key value, return false.
     *
     * @param r Record to insert into the table.
     * @return boolean true if the insertion succeeded, false otherwise
     */
    public boolean insert(Record r) {
        // TODO: implement this method
        if ((eNum*1.0)>=(length2/4.0)){
            //System.out.println("-------------------doubling:"+(length*2));
            doubling();
            
        }
    	int hk=r.getHashKey();
    	int hb=baseHash(hk);
    	int hs=stepHash(hk);
    	int fhk=hb;
        //System.out.println("---want to insert "+r.getKey()+" into "+fhk+" h1="+hb+" h2="+hs);
        while ( (table[fhk] != null) && (!(table[fhk].getKey().equals("d"))) ){
            if ((table[fhk].getHashKey()==hk)&&(r.getKey().equals(table[fhk].getKey())) ) {
                return false;
            }else{
                fhk=(fhk+hs)%length;
                if (fhk == hb) {
                    //doubling();
                    //if (doubleInsert(r)) {
                    //    return true;
                    //}
                    return false;
                }
            }
        }
        table[fhk]=r;
        //System.out.println("---insert "+r.getKey()+" into "+fhk+" h1="+hb+" h2="+hs);
        eNum++;
        return true;
    }

    /**
     * Delete a record from the table.
     *
     * Note: You'll have to find the record first unless you keep some
     * extra information in the Record structure.
     *
     * @param r Record to remove from the table.
     */
    public void remove(Record r) {
        // TODO: implement this method
    	int hk=r.getHashKey();
        //String rs=r.getKey();
    	int hb=baseHash(hk);
    	int hs=stepHash(hk);
    	int fhk=hb;
        while (table[fhk] != null){    
            if ( (table[fhk].getHashKey()==hk)&&(table[fhk].getKey().equals(r.getKey())) ) {
                table[fhk]=rmvd;
                //System.out.println("------delete "+fhk);
                eNum--;
                break;
            }else{
                fhk=(fhk+hs)%length;
                if (fhk == hb) {
                    break;
                }
            }
        }
    }
    

    /**
     * Find a record with a key matching the input.
     *
     * @param key to find
     * @return the matched Record or null if no match exists.
     */
    public Record find(String key) {
        // TODO: implement this method
        if (eNum==0){
            return null;
        }
        //System.out.println("Want to find:"+key);
    	int hk=toHashKey(key);
    	int hb=baseHash(hk);
    	int hs=stepHash(hk);
    	int fhk=hb;
        while (table[fhk] != null) {
            
            if ( (table[fhk].getHashKey()==hk)&&(table[fhk].getKey().equals(key)) ) {
                //System.out.println("-------------------It is in "+fhk);
                return table[fhk];
            }else{
                //System.out.println("-------------------not in h1="+hb+"; h2="+hs+"; current at:"+fhk+"; size="+length);
                fhk=(fhk+hs)%length;
                if (fhk == hb) {
                    return null;
                }
            }
        }
    	return null;
    }

    

    /**
     * Return the size of the hash table (i.e. the number of elements
     * this table can hold)
     *
     * @return the size of the table
     */
    public int size() {
        // TODO: implement this method
       return length2;
    }

    /**
     * Return the hash position of this key.  If it is in the table, return
     * the postion.  If it isn't in the table, return the position it would
     * be put in.  This value should always be smaller than the size of the
     * hash table.
     *
     * @param key to hash
     * @return the int hash
     */
    public int hash(String key) {
        // TODO: implement this method
    	boolean zdl=false;
    	int hk=toHashKey(key);
    	int hb=baseHash(hk);
    	int hs=stepHash(hk);
    	int fhk=hb;
        if (eNum==0){
            zdl=false;
        }else{
            while (table[fhk] != null) {
                if ( (table[fhk].getHashKey()==hk)&&(table[fhk].getKey().equals(key)) ){
                    return fhk;
                }else{
                    fhk=(fhk+hs)%length;
                    if (fhk == hb) {
                        break;
                    }
                }
            }
        }
    	if (!zdl){
        	fhk=hb;
            while ( (table[fhk] != null) && (!(table[fhk].getKey().equals("d"))) ){
                fhk=(fhk+hs)%length;
                if (fhk == hb) {
                    //doubling();
                    //return hash1(key);
                    return 0;
                }
            }
            return(fhk);
        }
    	return 0;
    }

    /*public int hash1(String key) {
        // TODO: implement this method
        int hk=toHashKey(key);
        int hb=baseHash(hk);
        int hs=stepHash(hk);
        int fhk=hb;
        while ( (table[fhk] != null) && (!(table[fhk].getKey().equals("d"))) ){
            fhk=(fhk+hs)%length;
            if (fhk == hb) {
                return 0;
            }
        }
        return(fhk);
    }*/

    /**
     * Convert a String key into an integer that serves as input to hash functions.
     * This mapping is based on the idea of a linear-congruential pseuodorandom
     * number generator, in which successive values r_i are generated by computing
     *    r_i = (A * r_(i-1) + B) mod M
     * A is a large prime number, while B is a small increment thrown in so that
     * we don't just compute successive powers of A mod M.
     *
     * We modify the above generator by perturbing each r_i, adding in the ith
     * character of the string and its offset, to alter the pseudorandom
     * sequence.
     *
     * @param s String to hash
     * @return int hash
     */
    public static int toHashKey(String s) {
        int A = 1952786893;
        int B = 367253;
        int v = B;

        for (int j = 0; j < s.length(); j++) {
            char c = s.charAt(j);
            v = A * (v + (int) c + j) + B;
        }

        if (v < 0) {
            v = -v;
        }
        return v;
    }

    /**
     * Computes the base hash of a hash key
     *
     * @param hashKey
     * @return int base hash
     */
    int baseHash(int hashKey) {
        // TODO: implement this method
        int temp=(int)  (length*    (   (   hashKey*(Math.sqrt(5)-1)/2 )%1)    );
       return temp;
    }

    /**
     * Computes the step hash of a hash key
     *
     * @param hashKey
     * @return int step hash
     */
    int stepHash(int hashKey) {
        // TODO: implement this method
        int temp;
    	temp=     (int)   (length*     ((hashKey*Math.PI/5)%1) )  ;

        if (((temp%2)==0)){temp--;}
    	if ((temp<=0)){
    		temp=1;
    	}
        return temp;
    }

    void doubling(){
        Record[] oldTable=table;
        int oldLength=length;
        //boolean cc=true;
        //while(cc){
    	   //length=2*length;
           length2=2*length2;
           while (length<length2){
                length=length*2;
           }
    	   table=new Record[length];
           eNum=0;
    	   for (int i=0;i<oldLength;i++){
            if ( (oldTable[i]!=null) && (!(oldTable[i].getKey().equals("d"))) ){
                //System.out.print("insert "+oldTable[i].getKey()+" which is "+i);
                boolean t=doubleInsert(oldTable[i]);
                //if (!t) {
                //    break;
                //}
            }
            //if (i==(oldLength-1)) {
            //   cc=false;
            //}
            }
        //}
        //System.out.println("-----------------------finish doubling ");	
    }

    public boolean doubleInsert(Record r) {
        int hk=r.getHashKey();
        int hb=baseHash(hk);
        int hs=stepHash(hk);
        int fhk=hb;
        while ( (table[fhk] != null) && (!(table[fhk].getKey().equals("d"))) ){
            fhk=(fhk+hs)%length;
            if (fhk == hb) {
                return false;
            }
        }
        table[fhk]=r;
        //System.out.println(" into "+fhk+" h1="+hb+" h2="+hs);
        eNum++;
        return true;
    }


    
}