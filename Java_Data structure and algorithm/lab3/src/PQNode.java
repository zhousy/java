package lab3;

public class PQNode<T> {
	
	T value;
	int key;
	public Handle h=new Handle();
	
	public PQNode(T value, int key){
		this.value=value;
		this.key=key;
	}
	
	public int getKey(){
		return this.key;
	}
	
	public void decrease(int nk){
		this.key=nk;
	}
	
	public T getValue(){
		return this.value;
	}
	/*
	public setH(int index){
		this.h.setI(index);
	}
	
	public setFH(Handle h){
		this.h=h;
	}
	*/
	public void swap(PQNode<T> other){
		int nk=this.h.getI();
		this.h.setI(other.h.getI());
		other.h.setI(nk);
		/*T cv=this.value;
		int ck=this.key;
		this.value=other.value;
		this.key=other.key;
		other.value=cv;
		other.key=ck;
		System.out.println(this.key+"<-->"+other.key);*/
	}
}
