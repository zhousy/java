package lab3;
import java.util.ArrayList;

   class ShortestPaths {

    class Point{
        Handle handle;
        Vertex vertex;
        int d; 
        Point pre; 
        int e;              
        Point(){}         
        Point(Vertex v){pre = null;d = maxInt;vertex = v;}
    }
    PriorityQueue<Point> heap = new PriorityQueue<Point>();
	int maxInt =2147483647;
	ArrayList<Point> vList = new ArrayList<Point>();
    
    public void init(PriorityQueue<Point> heap,Multigraph G, int startId){
        for (int i = 0; i< G.nVertices(); i++){
            vList.add(new Point());
        }
        for (int i = 0; i< G.nVertices(); i++){
            Vertex temp = G.get(i);
            Point p = new Point(temp);
            if (temp.id() == startId){
                p.d = 0;
                int id = temp.id();
                p.handle = heap.insert(0, p);
                vList.set(id, p);
            }
            else {
                int id = temp.id();
                p.handle = heap.insert(maxInt, p);
                vList.set(id, p);
            }
        }
    }
    
    public ShortestPaths(Multigraph G, int startId) {
    	init(heap,G,startId);
    	while (!heap.isEmpty()){
    		Point a = heap.extractMin();
    		if (a.d != maxInt){
    		Vertex.EdgeIterator neighbor = a.vertex.adj(); 
    		while (neighbor.hasNext()) {
    			Edge edge = neighbor.next();
    			Point b = vList.get(edge.to().id());
    			loose(b,a,edge);    			}
    		}
    	}
    }
    
    public void loose(Point a, Point b,Edge e){
    	if ((a.handle.index != -1) && heap.decreaseKey(a.handle, b.d + e.weight()) ){
    		a.d = b.d+ e.weight();
			a.e = e.id();
			a.pre = b;
            return;
    	}
    	if ((a.handle.index == -1) && ( (b.d + e.weight())< a.d) ){
    		a.d = b.d+ e.weight();
			a.e = e.id();
			a.pre = b;
            return;
    	}
    }

    public int[] returnPath(int endId) { 
        int k = 0;
        ArrayList<Integer> id = new ArrayList<Integer>();
    	Point end = vList.get(endId);
    	while (end.pre != null){
    		id.add(end.e);
    		end = end.pre;
    		k++;
    	}
        //int oldPath[]=id.toArray(new int[id.size()]);
    	int path[] = new int[k];
    	for (int i = k; i> 0; i--){
    		path[i-1] = id.get(k-i);
    	}
    	return path;
    }
}





