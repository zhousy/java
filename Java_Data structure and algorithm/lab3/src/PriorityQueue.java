package lab3;
import java.util.ArrayList;
/**
 * A priority queue class supporting operations needed for
 * Dijkstra's algorithm.
 */
class PriorityQueue<T> {
	
	ArrayList<PQNode<T>> heap;
	//int size;
    /**
     * Constructor
     */
    public PriorityQueue() {
    	this.heap=new ArrayList<PQNode<T>>();
    }

    /**
     * @return true iff the queue is empty.
     */
    public boolean isEmpty() {
    	if (this.heap.size()==0) {
    		return true;
    	}
    	return false;
    }

    /**
     * Insert a (key, value) pair into the queue.
     *
     * @return a Handle to this pair so that we can find it later.
     */
    Handle insert(int key, T value) {
    	this.heap.add(new PQNode<T>(value,key));
    	int n=this.heap.size()-1;
        //int n2=n;
    	Handle h=new Handle(n);
        this.heap.get(n).h=h;
        //System.out.println("awef");
    	//int current=(n+1)/2;
    	while (n>0){
    		if (this.heap.get(n).getKey()<this.heap.get((n-1)/2).getKey()){
    			//int nh=this.heap.get(n).h.getI();
    			//this.heap.get(n).h.setI(this.heap.get((n-1)/2).h.getI());
    			//this.heap.get((n-1)/2).h.setI(nh);
    			/*this.heap.get(n).swap(this.heap.get((n-1)/2));
                PQNode<T> temp=this.heap.get(n);
                PQNode<T> temp2=this.heap.get((n-1)/2);
                this.heap.remove(n);
                this.heap.add(n,temp2);
                this.heap.remove((n-1)/2);
                this.heap.add((n-1)/2,temp);*/
                //n2=n;
                swap(n,(n-1)/2);
    			n=(n-1)/2;
                //System.out.println(n);
    		}else{
                return this.heap.get(n).h;
                //break;
            }
    	}
    	return this.heap.get(n).h; //cannot find eroro
    }

    /**
     * @return the min key in the queue.
     */
    public int min() {
       return this.heap.get(0).getKey();
    }

    /**
     * Find and remove the smallest (key, value) pair in the queue.
     *
     * @return the value associated with the smallest key
     */
    public T extractMin() {
       T temp=this.heap.get(0).getValue();
       int n=this.heap.size()-1;
       /*this.heap.get(n).swap(heap.get(0));
       PQNode<T> tem2=this.heap.get(n);
       this.heap.remove(0);
       this.heap.add(0,tem2);
       
       */
       //this.heap.add(n,tem);
       //this.heap.remove(n);
       swap(0,n);
       this.heap.get(n).h.setI(-1);
       this.heap.remove(n);
       this.heapify(0);
       return temp;
    }
    public void swap(int i,int j){
        PQNode<T> tempi=this.heap.get(i);
        PQNode<T> tempj=this.heap.get(j);
        this.heap.remove(i);
        this.heap.add(i,tempj);
        this.heap.remove(j);
        this.heap.add(j,tempi);
        this.heap.get(i).h.setI(i);
        this.heap.get(j).h.setI(j);
    }
    public void heapify(int i){
    	int size=this.heap.size()-1;
    	int l=i*2+1;
    	int r=i*2+2;
    	int min;
    	if ( (l<size)&&(this.heap.get(l).getKey()<this.heap.get(i).getKey()) ) {
    		min=l;
    	}else{min=i;}
    	if ( (r<size)&&(this.heap.get(r).getKey()<this.heap.get(min).getKey()) ) {
    		min=r;
    	}
    	if (min != i) {
    	    /*this.heap.get(min).swap(heap.get(i));
            PQNode<T> temp=this.heap.get(min);
            PQNode<T> temp2=this.heap.get(i);
            this.heap.remove(min);
            this.heap.add(min,temp2);
            this.heap.remove(i);
            this.heap.add(i,temp);*/
            swap(min,i);
    	    heapify(min);
    	}
    }


    /**
     * Decrease the key to the newKey associated with the Handle.
     *
     * If the pair is no longer in the queue, or if its key <= newKey,
     * do nothing and return false.  Otherwise, replace the key with newkey,
     * fix the ordering of the queue, and return true.
     *
     * @return true if we decreased the key, false otherwise.
     */
    public boolean decreaseKey(Handle h, int newKey) {
    	if ((h.getI()==(-1))||(handleGetKey(h)<newKey)) {
    		return false;
    	}
    	this.heap.get(h.getI()).decrease(newKey);
    	return true;
    }

    /**
     * @return the key associated with the Handle.
     */
    public int handleGetKey(Handle h) {
        if (h.getI()==-1) {return 0;}
       return this.heap.get(h.getI()).getKey();
    }

    /**
     * @return the value associated with the Handle.
     */
    public T handleGetValue(Handle h) {
        if (h.getI()==-1) {return null;}
    	return this.heap.get(h.getI()).getValue();
    }

    /**
     * Print every element of the queue in the order in which it appears
     * (i.e. the array representing the heap)
     */
    public String toString() {
    	String s="";
    	for (int i=0;i<this.heap.size();i++){
    		s=s+"("+this.heap.get(i).getKey()+","+this.heap.get(i).getValue()+","+this.heap.get(i).h.getI()+")";
    	}
       return s;
    }
}
