package lab10;
import java.awt.Color;
import sedgewick.StdDraw;
/**
 * 
 * @author Shaoyang Zhou 440747 & Runbo Yang 439275
 * the object car used in game
 * We have complete the base goal and we also do 3 features for extra credit
 * a) Require the player to cross the stream
 * b) Allow multiple players, either by letting them take turns or having them
 *    race towards the goal
 * c) Every time a level/goal is reached, make the game more difficult by making 
 *    the cars move faster and spawning a larger number of them. 
 *
 */
public class Car {
	
	//x,y the position of the car
	double x;
	double y;
	
	/**
	 * Constractor of the car
	 * @param x the horizontal coordinate of the car
	 * @param y the vertical coordinate of the car
	 */
	public Car(double x,double y){
		this.x=x;
		this.y=y;
	}
	
    /**
     * define the car's movement
     */
	public void moving(){
		this.x+=0.1;
	}
	
	/**
	 * determine the car moves or not
	 * @param size the size of the map
	 * @return
	 */
	public boolean move(int size){
		if (this.x>size){
			return false;
		}
		return true;
	}
	
	/**
	 * get the car's horizontal coordinate
	 * @return
	 */
	public double getX(){
		return this.x;
	}
	
	/**
	 * get the car's vertical coordinate
	 * @return
	 */
	public double getY(){
		return this.y;
	}
	
	/**
	 * determine whether Frog f has a collision with this car
	 * @param f a Frog
	 * @return
	 */
	public boolean onTheCar(Frog f){
		if ((f.getX()<=(this.x+0.05))&&(f.getX()>=(this.x-0.05))&&(f.getY()<=(this.y+0.025))&&(f.getY()>=(this.y-0.025))){
			return true;
		}
		return false;
	}
	
	/**
	 * draw the car
	 */
	public void drawCar(){
		StdDraw.setPenColor(Color.BLACK);
		StdDraw.filledRectangle(this.x+0.05, this.y+0.025, 0.025, 0.0125);
		StdDraw.filledRectangle(this.x+0.05, this.y-0.025, 0.025, 0.0125);
		StdDraw.filledRectangle(this.x-0.05, this.y+0.025, 0.025, 0.0125);
		StdDraw.filledRectangle(this.x-0.05, this.y-0.025, 0.025, 0.0125);
		StdDraw.setPenColor(Color.RED);
		StdDraw.filledRectangle(this.x, this.y, 0.05, 0.025);
		StdDraw.setPenColor(Color.PINK);
		StdDraw.filledRectangle(this.x, this.y, 0.025, 0.0125);

	}
	/*public static void main(String[] args) {
		Car a= new Car(0.5,0.5);
		a.drawCar();
	}*/
}