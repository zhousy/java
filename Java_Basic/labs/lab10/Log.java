package lab10;
import java.awt.Color;
import sedgewick.StdDraw;
/**
 * 
 * @author Runbo Yang 439275 & Shaoyang Zhou 440747
 * the object log used in game
 */
public class Log {
	//x,y the position of the log
	double x;
	double y;
	
	/**
	 * Constrator of the log
	 * @param x the horizontal coordinate of the log
	 * @param y the vertical coordinate of the log
	 */
	public Log(double x,double y){
		this.x=x;
		this.y=y;
	}
	
	/**
	 * define the log's movement
	 */
	public void moving(){
		this.x-=0.1;
	}
	
	/**
	 * determine whether the log is out of the scale of the map or not
	 * @return
	 */
	public boolean move(){
		if (this.x<0){
			return false;
		}
		return true;
	}
	
	/**
	 * get the horizontal coordinate of the log
	 * @return
	 */
	public double getX(){
		return this.x;
	}
	
	/**
	 * get the vertical coordinate of the log
	 * @return
	 */
	public double getY(){
		return this.y;
	}
	
	/**
	 * determine whether Frog f is on the log or not
	 * @param f Frog f
	 * @return
	 */
	public boolean onTheLog(Frog f){
		if ((f.getX()<=(this.x+0.05))&&(f.getX()>=(this.x-0.05))&&(f.getY()<=(this.y+0.025))&&(f.getY()>=(this.y-0.025))){
			return true;
		}
		return false;
	}
	
	/**
	 * draw the log
	 */
	public void drawLog(){
		StdDraw.setPenColor(new Color(165,42,42));
		StdDraw.filledRectangle(this.x, this.y, 0.04, 0.015);

	}
	public static void main(String[] args) {
		Log a= new Log(0.5,0.5);
		a.drawLog();
	}
}