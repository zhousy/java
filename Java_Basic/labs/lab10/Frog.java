package lab10;
import java.awt.Color;
import sedgewick.StdDraw;
/**
 * 
 * @author Runbo Yang 439275 & Shaoyang Zhou 440747
 * The object frog used in game.
 * We have complete the base goal and we also do 3 features for extra credit
 * a) Require the player to cross the stream
 * b) Allow multiple players, either by letting them take turns or having them 
 *    race towards the goal
 * c) Every time a level/goal is reached, make the game more difficult 
 *    by making the cars move faster and spawning a larger number of them. 
 */
public class Frog {
	//x,y the position of the Frog
	//num the number of the Frog
	double x;
	double y;
	int num;
	
	/**
	 * Constractor of the Frog
	 * @param x the horizontal coordinate of the Frog
	 * @param y the vertical coordinate of the Frog
	 * @param number the number of the Frog
	 */
	public Frog(double x,double y,int number){
		this.x=x;
		this.y=y;
		this.num=number;
	}
	
	/**
	 * define the Frog's movement towards up direction
	 */
	public void up(){
		this.y+=0.2;
	}
	
	/**
	 * determine whether the Frog moves into the river or not
	 * @param size the size of the map
	 * @return
	 */
	public boolean inTheRiver(int size){
		int river=((int)((size-0.4)*10))/4;
		if ((this.y<size-0.2)&&(this.y>size-0.2-0.2*river)){
			return true;
		}
		return false;
	}
	
	/**
	 * determine whether the Frog is in the scale of the map
	 * @param size the size of the map
	 * @return
	 */
	public boolean inScale(int size){
		if ((this.x>=0)&&(this.x<=size)){
			return true;
		}
		return false;
	}
	
	/**
	 * define the Frog's movement towards down direction 
	 */
	public void down(){
		this.y-=0.2;
	}
	
	/**
	 * define the Frog's movement towards left direction
	 */
	public void left(){
		this.x-=0.2;
	}
	
	/**
	 * define the Frog's movement towards right direction
	 */
	public void right(){
		this.x+=0.2;
	}
	/*public void move(double x,double y){
		this.x+=x;
		this.y+=y;
	}*/
	
	/**
	 * define the movement that Frog moves with the logs
	 */
	public void moving(){
		this.x-=0.1;
	}
	
	/**
	 * get the horizontal coordinate of the Frog
	 * @return
	 */
	public double getX(){
		return this.x;
	}
	
	/**
	 * get the vertical coordinate of the Frog
	 * @return
	 */
	public double getY(){
		return this.y;
	}
	
	/**
	 * determine whether the Frog reaches the goal
	 * @param size the size of the map
	 * @return
	 */
	public boolean inGoal(int size){
		if (this.y>(size-0.2)){
			return true;
		}
		return false;
	}
    
	/**
	 * draw the Frog
	 */
	public void drawFrog(){
		StdDraw.setPenColor(new Color(0,100,0));
		StdDraw.filledCircle(this.x, this.y, 0.05);
		StdDraw.setPenColor(Color.WHITE);
		StdDraw.text(this.x, this.y, ""+this.num);	
	}

}