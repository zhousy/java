package lab10;

import cse131.ArgsProcessor;
import lab10.ControlDemo;

/**
 * 
 * @author shaoyang zhou 440747 runbo yang 439275
 * This is the main program you can keep playing game endless once you finished 
 * or game over it will ask you to start a totally new one.
 * 
 * We have complete the base goal and we also do 3 features for extra credit
 * a) Require the player to cross the stream
 * b) Allow multiple players, either by letting them take turns or having them 
 *    race towards the goal
 * c) Every time a level/goal is reached, make the game more difficult by making 
 *    the cars move faster and spawning a larger number of them. 

 */
public class FroggerMain {
	
	
	public static void main(String[] args) {
		while (true){
		ArgsProcessor ap = new ArgsProcessor(args);
		int player=0;
		while ((player!=1)&&(player!=2)) {
			player=ap.nextInt("Input number of players:");
		}
		String p1=ap.nextString("Input name of player1:");
		String p2="";
		if (player==2){
			p2=ap.nextString("Input name of player2:");
		}
		
		
		if (player==1){
			 ControlDemo g = new ControlDemo(p1);
			 g.playGame();
		}
		if (player==2){
			ControlDemo g= new ControlDemo(p1,p2);
			g.playGame();
		}
		}
	}
}
