package lab10;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.LinkedList;

import sedgewick.StdDraw;
import lab10.ArcadeKeys;
import lab10.FroggerGame;

/**
 * 
 * @author Shaoyang Zhou 440747 & Runbo Yang 439275
 * This is the main control codes. We change nearly all this file.
 * We have complete the base goal and we also do 3 features for extra credit
 * a) Require the player to cross the stream
 * b) Allow multiple players, either by letting them take turns or having them race towards the goal
 * c) Every time a level/goal is reached, make the game more difficult by making the cars move faster and spawning a larger number of them. 
 *
 */
public class ControlDemo implements FroggerGame {
    //level the level of the game
	//player the number of the player who will join the game
	//p1,p2 the names of the players
	//live1,live2 the number of lives that a player has
	//s1,s2 the score that a player got
	int level=0;
	int player=0;
	String p1="";
	String p2="";
	int live1=5;
	int live2=5;
	int s1=0;
	int s2=0;
	
	/**
	 * set up a sign to determine whether the game is over or not
	 */
	public boolean flag=true;
	
	/**
	 * draw the background
	 */
	public void drawBackground(){
		int size=this.level/2+1;
		StdDraw.setScale(0, size);
		int river=((int)((size-0.4)*10))/4;
		int road=((int)((size-0.4)*10))/2-river;
		StdDraw.setPenColor(Color.YELLOW);
		StdDraw.filledRectangle(size/2.0, size-0.1, size/2.0, 0.1);
		StdDraw.setPenColor(Color.BLUE);
		StdDraw.filledRectangle(size/2.0, size-0.2-(river*0.1), size/2.0, river*0.1);
		StdDraw.setPenColor(Color.DARK_GRAY);
		StdDraw.filledRectangle(size/2.0, size-0.2-(river*0.2)-(road*0.1), size/2.0, road*0.1);
		StdDraw.setPenColor(Color.GREEN);
		StdDraw.filledRectangle(size/2.0, size-0.2-(river*0.2)-(road*0.2)-0.1, size/2.0, 0.1);
		drawInfo();
	}
	
	/**
	 * Constractor of ControlDemo with 1 player
	 * @param p1 player's name
	 */
	public ControlDemo(String p1){
		this.player=1;
		this.p1=p1;
	}
	
	/**
	 * Constractor of ControlDemo with 2 players
	 * @param p1 player1's name
	 * @param p2 player2's name
	 */
	public ControlDemo(String p1,String p2){
		this.p1=p1;
		this.p2=p2;
		this.player=2;
	}

	@Override
	/**
	 * implement the game
	 */
	public void playGame() {
		int size=this.level/2+1;;
		int river=((int)((size-0.4)*10))/4;
		int road=((int)((size-0.4)*10))/2-river;
		ArrayList<Car> carList=new ArrayList<Car>();
		ArrayList<Log> logList=new ArrayList<Log>();
		Frog f1=new Frog(0.25,0.1,1);
		Frog f2=null;
		if (this.player==2){
			f2= new Frog(0.75,0.1,2);
		}
		boolean winf=false;
		boolean[] on=new boolean[8];
		for (int i=1;i<8;i++){
			on[i]=false;
		}
		while (this.flag) {
			
			//1

			if (ArcadeKeys.isKeyPressed(0, ArcadeKeys.KEY_UP)) {

				if ((!on[0])&&(f1 != null)) {
					f1.up();
				}
				on[0] = true;
			} else {
				if (on[0]) {
					
				}
				on[0] = false;
			}
			
			if (ArcadeKeys.isKeyPressed(0, ArcadeKeys.KEY_DOWN)) {

				if ((!on[1])&&(f1 != null)) {
					f1.down();
				}
				on[1] = true;
			} else {
				if (on[1]) {
					
				}
				on[1] = false;
			}
			
			if (ArcadeKeys.isKeyPressed(0, ArcadeKeys.KEY_LEFT)) {

				if ((!on[2])&&(f1 != null)) {
					f1.left();
				}
				on[2] = true;
			} else {
				if (on[2]) {
					
				}
				on[2] = false;
			}
			
			if (ArcadeKeys.isKeyPressed(0, ArcadeKeys.KEY_RIGHT)) {

				if ((!on[3])&&(f1 != null)) {
					f1.right();
				}
				on[3] = true;
			} else {
				if (on[3]) {
					
				}
				on[3] = false;
			}
			
			
			//2
			
			if (ArcadeKeys.isKeyPressed(1, ArcadeKeys.KEY_UP)) {

				if ((!on[4])&&(f2 != null)) {
					f2.up();
				}
				on[4] = true;
			} else {
				if (on[4]) {
					
				}
				on[4] = false;
			}
			
			if (ArcadeKeys.isKeyPressed(1, ArcadeKeys.KEY_DOWN)) {

				if ((!on[5])&&(f2 != null)) {
					f2.down();
				}
				on[5] = true;
			} else {
				if (on[5]) {
					
				}
				on[5] = false;
			}
			
			if (ArcadeKeys.isKeyPressed(1, ArcadeKeys.KEY_LEFT)) {

				if ((!on[6])&&(f2 != null)) {
					f2.left();
				}
				on[6] = true;
			} else {
				if (on[6]) {
					
				}
				on[6] = false;
			}
			
			if (ArcadeKeys.isKeyPressed(1, ArcadeKeys.KEY_RIGHT)) {

				if ((!on[7])&&(f2 != null)) {
					f2.right();
				}
				on[7] = true;
			} else {
				if (on[7]){
					
				}
				on[7] = false;
			}
			
			StdDraw.clear();
			drawBackground();
			for (int i=0;i<carList.size();i++){
				carList.get(i).moving();
				if ((f1!=null)&&(carList.get(i).onTheCar(f1))){
					f1=null;
					dead(1);
				}
				if ((f2!=null)&&(carList.get(i).onTheCar(f2))){
					f2=null;
					dead(2);
				}
			}
			for (int i=0;i<road;i++){
				int r=(int)(Math.random()*10);
				if ((r*10)<this.level){
					carList.add(new Car(0,0.2+i*0.2+0.1));
				}
			}
			for (int i=0;i<carList.size();i++){
				if (!carList.get(i).move(size)) {
					carList.remove(i);
					i--;
				}else{
					carList.get(i).drawCar();
				}
			}
			boolean f1f=false;
			boolean f2f=false;
			for (int i=0;i<logList.size();i++){
				logList.get(i).moving();
				if ((f1!=null)&&(logList.get(i).onTheLog(f1))){
					f1.moving();
					f1f=true;
				}
				if ((f2!=null)&&(logList.get(i).onTheLog(f2))){
					f2.moving();
					f2f=true;
				}
			}
			if ((f1!=null)&&(!f1f)&&(f1.inTheRiver(size))){
				f1=null;
				dead(1);
			}
			if ((f2!=null)&&(!f2f)&&(f2.inTheRiver(size))){
				f2=null;
				dead(2);
			}
			for (int i=0;i<river;i++){
				int r=(int)(Math.random()*10);
				if ((r*4)>this.level){
					logList.add(new Log(size,size-0.2-i*0.2-0.1));
				}
			}
			for (int i=0;i<logList.size();i++){
				if (!logList.get(i).move()){
					logList.remove(i);
					i--;
				}else{
					logList.get(i).drawLog();
				}
			}
			if (f1!=null){
				if ((f1.inGoal(size))){
					f1=null;
					win(1);
					winf=true;
				}else{
				if (f1.inScale(size)) {
					f1.drawFrog();
				}else{
					f1=null;
					dead(1);
				}}
			}
			if (f2!=null){
				if ((f2.inGoal(size))){
					f2=null;
					win(2);
					winf=true;
				}else{
				if (f2.inScale(size)) {
					f2.drawFrog();
				}else{
					f2=null;
					dead(2);
				}}
			}
			if (((f1==null)&&(f2==null))||((f1==null)&&(this.player==1))){
				//reset
				if (winf) {
					this.level++;
					StdDraw.setPenColor(Color.PINK);
					StdDraw.filledSquare(size/2.0, size/2.0, size/2.0);
					StdDraw.setPenColor(Color.BLACK);
					StdDraw.setPenRadius(0.1);
					StdDraw.text(size/2.0, size/2.0,"Level UP!");
					StdDraw.show(1000);
					winf=false;
				}
				size=this.level/2+1;;
				river=((int)((size-0.4)*10))/4;
				road=((int)((size-0.4)*10))/2-river;
				carList=new ArrayList<Car>();
				logList=new ArrayList<Log>();
				f1=new Frog(0.25*size,0.1,1);
				f2=null;
				if (this.player==2){
					f2= new Frog(0.75*size,0.1,2);
				}
			}
			StdDraw.show((250/(level+1)));
		}
		
	}
	
	/**
	 * the operation after one player reaches the goal
	 * @param i the sign of the player
	 */
	public void win(int i){
		//f=null;
		if (i==1){
			this.s1+=(this.level+1);
			this.live1++;
			//if ((this.level==9)&&(this.player==2)){
			//	gameOver("P1 win! GAME OVER!");
			//}
		}else{
			this.s2+=(this.level+1);
			this.live2++;
			//if ((this.level==9)&&(this.player==2)){
			//	gameOver("P2 win! GAME OVER!");
			//}
		}
	}
	
	/**
	 * draw the picture of gameOver
	 * @param s output the result of the game
	 */
	public void gameOver(String s){
		int size=this.level/2+1;
		StdDraw.setPenColor(Color.PINK);
		StdDraw.filledSquare(size/2.0, size/2.0, size/2.0);
		StdDraw.setPenColor(Color.BLACK);
		StdDraw.setPenRadius(0.1);
		StdDraw.text(size/2.0, size/2.0,s);
		StdDraw.show(1000);
		this.flag=false;
	}
	
	/**
	 * the operation after one player dead in the game
	 * @param i the sign of the player
	 */
	public void dead(int i){
		//f=null;
		if (i==1){
			this.live1--;
		}else{
			this.live2--;
		}
		if (this.live1<0) {
			if ((this.live2>=0)&&(this.player==2)){
				gameOver("P2 win! GAME OVER!");
			}else{
				gameOver("GAME OVER!");
			}
		}else if (this.live2<0) {
			if (this.live1>=0){
				gameOver("P1 win! GAME OVER!");
			}
		}else if ((this.live1<0)&&(this.player==1)){
			gameOver("GAME OVER!");
		}

		
		
	}
	
	/**
	 * draw the information about the game
	 */
	public void drawInfo(){
		int size=this.level/2+1;
		StdDraw.setPenColor(Color.BLACK);
		if (this.player==1){
			StdDraw.text(0.5*size, 0, "S1:"+this.s1+" P1:"+this.p1+" life:"+this.live1+" level:"+this.level);
		}
		if (this.player==2){
			StdDraw.text(0.5*size, 0, "S1:"+this.s1+" P1:"+this.p1+" life:"+this.live1+" S2:"+this.s2+" P2:"+this.p2+" life:"+this.live2+" level:"+this.level);
		}
	}

	@Override
	/**
	 * get the game's name
	 */
	public String getGameName() {
		return "Froooooger";
	}

	@Override
	/**
	 * get the team members' names
	 */
	public String[] getTeamMembers() {
		String[] members = { "Shaoyang Zhou", "Runbo Yang" };
		return members;
	}
	public static void main(String[] args) {
		ControlDemo a=new ControlDemo("1","2");
		a.drawBackground();
	}

}