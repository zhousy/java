package lab7;
/**
 * 
 * @author Shaoyang Zhou 440747 Runbo Yang 439275
 * Class student for lab7
 * 
 *
 */
public class Student {
	private String firstName;
	private String lastName;
	private int studentID;
	private int credits;
	private double gPA;
//---------------conductor-------------------	
	/**
	 * 
	 * @param firstname //TA: Describe these parameters!
	 * @param lastname
	 * @param studentid
	 */
	
	public Student(String firstName,String lastName,int studentID){
		this.firstName=firstName;
		this.lastName=lastName;
		this.studentID=studentID;
		this.credits=0;
		this.gPA=0;
	}
	/**
	 * This conductor is used to the createBaby method
	 * TA: -1, should not need another constructor for the createBaby method. Use the first one.
	 * @param firstname
	 * @param lastname
	 * @param studentid
	 * @param gPA
	 * @param credits
	 */
	public Student(String firstName,String lastName,int studentID,double gPA,int credits){
		this.firstName=firstName;
		this.lastName=lastName;
		this.studentID=studentID;
		this.credits=credits;
		this.gPA=gPA;
	}
//----------------accessor__________________
	/**
	 * 
	 * @return
	 */
	public String getName() {
		return this.firstName+" "+this.lastName;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getLastName(){
		return this.lastName;
	}

	/**
	 * 
	 * @return
	 */
	public int getStudentID() {
		return this.studentID;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getCredits() {
		return this.credits;
	}
	
	/**
	 * 
	 * @return
	 */
	public double getGPA() {
		return this.gPA;
	}
//-----------------------------------------------
	/**
	 * make the object into a string
	 */

	public String toString(){
		return this.firstName+" "+this.lastName+" "+this.studentID;
	}
	/**
	 * the class of the student
	 * @return
	 */
	public String getClassStanding(){
		if (this.credits<30){
			return "Freshman";
		}
		if (this.credits<60){
			return "Sophomore";
		}
		if (this.credits<90){
			return "Junior";
		}
		return "Senior";
	}
	/**
	 * wether this student is graduated
	 * @return
	 */
	public boolean isGraduated(){
		if (this.credits>=120) { //TA: Can you think of a way to shorten this to one line? -1
			return true;
		}
		return false;
	}
	/**
	 * whether current student has high GPA than the other one
	 * @param other,a object that is the other student
	 * @return
	 */
	public boolean hasHigherAverage(Student other){
		if (this.gPA>other.getGPA()){ //TA: Same as above.
			return true;
		}
		return false;
	}
	/**
	 * update the GPA and credits of current student
	 * @param GPA
	 * @param credit
	 */
	public void submitGrade(double GPA,int credit){
		
		//update the new GPA and add the credit to the total credits
		
		this.gPA=Math.round((this.gPA*this.credits+GPA*credit)/(this.credits+credit)*1000)/1000.0;
		this.credits+=credit;
	}
	
	/**
	 * compute the tuition to the current student
	 * @return
	 */
	
	public double computeTuition(){
		if (this.credits<18){
			return 22850.0;
		}
		long numOfSemester=Math.round((this.credits/18.0));
		return 22850.0*numOfSemester;
	}
	/**
	 * 
	 * @param other,an object that is the other student
	 * @return
	 */
	public Student createBaby(Student other){
		
		//GPA by average
		double gPA=Math.round((this.gPA+other.getGPA())/2*100)/100.0;
		
		//credits by the higher one
		int credits;
		if (this.credits>other.getCredits()) {
			credits=this.credits;
		}else{
			credits=other.getCredits();
		}
		
		//name from full name
		String firstName=this.getName();
		String lastName=other.getName();
		int studentID=(int)((Math.random()+1)*100000);
		return new Student(firstName,lastName,studentID,gPA,credits);
	}
	
}
