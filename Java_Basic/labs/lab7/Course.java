package lab7;

/**
 * TA: Will Strauss
 * 95/100
 * *Description of class goes here* -1
 * @author Runbo Yang 439275, Shaoyang Zhou 440747
 *
 */
public class Course {
	String name;
	int credits;
	int numberOfSeats;
	Student[] students;
//---------------conductor-------------------	
	/**
	 * 
	 * @param name, the name of a course
	 * @param credits, how many credits does this course have
	 * @param seats, the number of students that this course can contain
	 */
	public Course(String name,int credits, int seats){
		this.name=name;
		this.credits=credits;
		this.numberOfSeats=seats;
		this.students=new Student[seats];
	}
//----------------accessor__________________
	/**
	 * TA: Put comments here before all the methods with a description -1
	 * @return TA: What is being returned? Describe it! -1
	 */
	public int getRemainingSeats(){
		return this.numberOfSeats;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getName(){
		return this.name;
	}
//-----------------------------------------------
	/**
	 * 
	 */
	public String toString(){
		return this.name+" "+this.credits;
	}
//-----------------------------------------------	
	/**
	 * 
	 * @param student, an object that can be added into this course
	 * @return
	 */
	public boolean addStudent(Student student){
		if (this.numberOfSeats>0) {
			this.numberOfSeats--;
			this.students[this.students.length-this.numberOfSeats-1]=student;
			return true;
		}
		return false;
	}
	
	/**
	 * print the student in roster
	 */
	public void printRoster(){
		for (int i=0;i<this.students.length-this.numberOfSeats;i++){
			System.out.println(this.students[i].toString());
		}
	}
	
	/**
	 * compute the GPA of all student
	 * @return
	 */
	public double averageGPA(){
		double sum=0;
		int amount=0;
		for (int i=0;i<this.students.length-this.numberOfSeats;i++){
			sum += this.students[i].getGPA();
			amount++;
		}
		return sum/amount;
		
	}
	
}