package lab4;

import sedgewick.StdAudio;
import sedgewick.StdDraw;
import sedgewick.StdIn;
import cse131.ArgsProcessor;

import java.awt.Color;


public class BumpingBalls {
	static boolean flagg;
	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		StdDraw.setPenColor(Color.BLACK);
		//ArgsProcessor.useStdInput("Choose music");
		int b=ap.nextInt("Ball amount:");
		int t=ap.nextInt("Iteration times:");
		double a[][]=new double[b][2];
		double v[][]=new double[b][2];
		double r[]=new double[b];
		for (int i=0;i<b;i++){
			boolean fl=true;
			a[i][0]=Math.random();
			a[i][1]=Math.random();
			v[i][0]=Math.random()/100+.01;
			v[i][1]=Math.random()/100+.01;
			r[i]=Math.random()/100+.05;
			if (i>0){
				while (fl==true) {
					fl=false;
					for (int j=0;j<i;j++){
						if(dis(i,j,a)<(r[i]+r[j])){
							fl=true;
							a[i][0]=Math.random();
							a[i][1]=Math.random();
						}
						
					}
				}
			}
		}

		int shwt=50;
	//	shwt=0;
		for (int k=0;k<t;k++){
			StdDraw.clear();
			for(int i=0;i<b;i++){
				for(int j=i+1;j<b;j++){
					flagg=true;
					meet(i,j,a,r,v);

				}
			}
			
			dododo(b,a,v);
			
			StdDraw.picture(0.5, 0.5, "images/cong.jpg");
			
			for (int i=0;i<b;i++){
				StdDraw.filledCircle(a[i][0], a[i][1], r[i]);
				
			}
			if (!flagg){
				StdAudio.play("sound/boing2.au");
			}
			StdDraw.show(shwt);

		}
		
	}
	
	public static double dis(int i,int j,double a[][]){
		double tem=Math.sqrt(Math.pow(a[i][0]-a[j][0],2)+Math.pow(a[i][1]-a[j][1], 2));
		
		return tem;
	}
	public static void meet(int i,int j,double a[][],double r[],double v[][]){
		//boolean flag=true;
		if ((a[i][0]>=1)||(a[i][0]<=0)){
			chg(i,0,v);
			flagg=false;
		}
		if(	(a[j][0]>=1)||(a[j][0]<=0)) {
			chg(j,0,v);
			flagg=false;
		}
		if ((a[i][1]>=1)||(a[i][1]<=0)) {
			chg(i,1,v);
			flagg=false;
		}
		if ((a[j][1]>=1)||(a[j][1]<=0)){
			chg(j,1,v);
			flagg=false;
		}
		if (flagg==true){
			if ( dis(i,j,a)<=(r[i]+r[j])) {
				chg(i,0,v);
				chg(i,1,v);
				chg(j,0,v);
				chg(j,1,v);
				flagg=false;
				//double a1[]=StdAudio.read("sound/boing.au");

				//StdAudio.play(aaa);
			}
		}
		
		//if (!flag){
			//double a1[]=StdAudio.read("sound/boing.au");
			//StdAudio.play(a1);
			//StdAudio.play("sound/boing.au");
		//}
		
	}
	
	public static void dododo(int b, double a[][],double v[][]){
		for (int i=0;i<b;i++){
			a[i][0]=a[i][0]+v[i][0];
			a[i][1]=a[i][1]+v[i][1];
		}
	}
	
	public static void chg(int i,int c,double v[][]){
		v[i][c]=-1*v[i][c];
	}

}
