package lab2;

public class Circle implements Measurable {
	
	double centerX;
	double centerY;
	double radius;
	
	public Circle(double x,double y,double r){
		this.centerX=x;
		this.centerY=y;
		this.radius=r;
	}

	@Override
	public double area() {
		// TODO Auto-generated method stub
		return this.radius*this.radius*Math.PI;
	}

	@Override
	public double perimeter() {
		// TODO Auto-generated method stub
		return 2*this.radius*Math.PI;
	}

}
