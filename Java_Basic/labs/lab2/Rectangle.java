package lab2;

public class Rectangle implements Measurable {
	
	double centerX;
	double centerY;
	double length;
	double width;
	
	public Rectangle(double x,double y,double l,double w){
		this.centerX=x;
		this.centerY=y;
		this.width=w;
		this.length=l;
	}

	@Override
	public double area() {
		// TODO Auto-generated method stub
		return this.width*this.length;
	}

	@Override
	public double perimeter() {
		// TODO Auto-generated method stub
		return 2*(this.width+this.length);
	}

}
