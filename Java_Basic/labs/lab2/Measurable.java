package lab2;

public interface Measurable {
	
	public double area();
	public double perimeter();

}
