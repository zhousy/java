package lab2;

import cse131.ArgsProcessor;

public class RPS2 {
	static int p1w,p2w;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		int t=ap.nextInt("Input times:");
		System.out.println((8 * 4 + 9 / 2));
		int p1,p2;
		p1w=p2w=0;
		double p1r;
		
		for (int i=0;i<t;i++){
			p1r=Math.random();
			p1=((int)(p1r*3))+1;
			p2=i%3+1;
			
			
			
			System.out.println("P2:"+p2+"; P1 "+p1);
			System.out.println(compare(p1,p2));
		}
		
		System.out.println("P1 percentage: "+(Math.round(p1w/(t*1.0)*100)/100.0));
		System.out.println("P2 percentage: "+(Math.round(p2w/(t*1.0)*100)/100.0));

	}
	public static String compare(int p1, int p2){
		String s="";
		if (p1>p2) {
			p1w++;
			s="P1 win!";
			if ((p2==1)&&(p1==3)) {
				p1w--;
				p2w++;
				s="P2 win!";
			}
		}
		
		if (p1<p2) {
			p2w++;
			s="P2 win!";
			if ((p1==1)&&(p2==3)){
				p2w--;
				p1w++;
				s="P1 win!";
			}
		}
		
		if (p1==p2) {
			s="No winner";
		}
		return s;
	}

}
