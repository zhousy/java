package lab2;

import java.util.LinkedList;

import lab2.Circle;
import lab2.Measurable;
import lab2.Rectangle;

public class TestList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		LinkedList<Measurable> list=new LinkedList<Measurable>(); 
		list.add(new Rectangle(1,1,3,4));
		list.add(new Circle(1,1,1));
		System.out.println(list.get(0).perimeter()+" "+list.get(1).perimeter());

	}

}
