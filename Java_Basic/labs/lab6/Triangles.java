package lab6;

import sedgewick.StdDraw;

public class Triangles {

	public static void main(String[] args) {
		// TODO Auto-generated method stub		TA: this should be deleted
		//compute the first triangle's position by store in array x and y
		double[] x={0.5,0,1};
		double[] y={Math.sqrt(3)/2,0,0};
		
		//call the method and draw the triangles and set the amount of recursions layers is 0 for the first step
		drawTriangles(x,y,0);

	}
	
	/**
	 * To draw the triangles using recursion
	 * @param x a double array that contains the x-position of the current biggest triangle
	 * @param y a double array that contains the y-position of the current biggest triangle
	 * @param i to count the amount of recursion layers
	 * 
	 */
	public static void drawTriangles(double[] x,double[] y,int i){
		// 7 is amount of the recursion layers
		if (i<7){
	
			StdDraw.polygon(x, y);   //draw the biggest triangle
			
			//Compute and draw the central triangle
			double[] xMiddle={(x[0]+x[1])/2,(x[0]+x[2])/2,(x[1]+x[2])/2};
			double[] yMiddle={(y[0]+y[1])/2,(y[0]+y[2])/2,(y[1]+y[2])/2};
			StdDraw.polygon(xMiddle, yMiddle);
			
			//compute the position for next layer and call method itself
			
			double[] subTriangle1XPosition={x[0],xMiddle[0],xMiddle[1]};
			double[] subTriangle1YPosition={y[0],yMiddle[0],yMiddle[1]};
			drawTriangles(subTriangle1XPosition,subTriangle1YPosition,i+1);
		
			double[] subTriangle2XPosition={xMiddle[0],x[1],xMiddle[2]};
			double[] subTriangle2YPosition={yMiddle[0],y[1],yMiddle[2]};
			drawTriangles(subTriangle2XPosition,subTriangle2YPosition,i+1);
		
			double[] subTriangle3XPosition={xMiddle[1],xMiddle[2],x[2]};
			double[] subTriangle3YPosition={yMiddle[1],yMiddle[2],y[2]};
			drawTriangles(subTriangle3XPosition,subTriangle3YPosition,i+1);
		
		}
		
	}

}
