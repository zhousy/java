package lab3;

import cse131.ArgsProcessor;

public class MineSweeper {

	public static void main (String[] args) {
		
		//
		// Do not change anything between here ...
		//
		ArgsProcessor ap = new ArgsProcessor(args);
		int cols = ap.nextInt("How many columns?");
		int rows = ap.nextInt("How many rows?");
		double percent = ap.nextDouble("What is the probability of a bomb?");
		//
		// ... and here
		//
		//  Your code goes below these comments
		//
		double r;
		int m=rows;
		int n=cols;
		int t;
		int map[][]=new int[m+2][n+2];
		int fl[]={0,-1};
		for (int i=1;i<=m;i++){
			for (int j=1;j<=n;j++){
				r=Math.random();
				t=0;
				if (r<percent) {
					t=1;
				}
				map[i][j]=fl[t];
			}
		}
		
		for (int i=1;i<=m;i++){
			for (int j=1;j<=n;j++){
				if (map[i][j]==-1){
					fillin(i,j,map,m,n);
				}
			}
		}
		
		for (int i=1;i<=m;i++){
			for(int j=1;j<=n;j++){
				if (map[i][j] == -1) {
					System.out.print(" *");
				}
				else {
					System.out.print(" .");
				}
			}
			System.out.print("	");
			for(int k=1;k<=n;k++){
				if (map[i][k] == -1){
					System.out.print(" *");
				}
				else {
					System.out.print(" "+map[i][k]);
				}
			}
			System.out.println();
		}
		
	}
	
	public static void fillin(int ii,int jj, int a[][],int mm,int nn){
		if (a[ii-1][jj-1] != -1 ){
			a[ii-1][jj-1]++;
		}
		if (a[ii][jj-1] != -1 ){
			a[ii][jj-1]++;
		}
		if (a[ii-1][jj] != -1 ){
			a[ii-1][jj]++;
		}
		if (a[ii-1][jj+1] != -1 ){
			a[ii-1][jj+1]++;
		}
		if (a[ii+1][jj-1] != -1 ){
			a[ii+1][jj-1]++;
		}
		if (a[ii][jj+1] != -1 ){
			a[ii][jj+1]++;
		}
		if (a[ii+1][jj] != -1 ){
			a[ii+1][jj]++;
		}
		if (a[ii+1][jj+1] != -1 ){
			a[ii+1][jj+1]++;
		}
	}
	
}
