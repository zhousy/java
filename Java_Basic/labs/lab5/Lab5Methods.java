package lab5;

/**
 * 
 * TA-ShreyA 
 * Grade: 98/100
 * Vague Variable names -2
 * 
 *
 */
public class Lab5Methods {
	/**
	 * multPos the product of two positive integers
	 * @param j a positive integer
	 * @param k a positive integer
	 * @return product, the product j*k
	 */
	public static int multPos(int j,int k){
		int product = 0;
		if(j > 0 && k > 0){
			for(int i = 0;i < k;i++){
				product = product + j;
			}
		}
		return product;
	}

	/**
	 * mult the product of two integers
	 * @param j an integer
	 * @param k an integer
	 * @return product, the product j*k
	 */
	public static int mult(int j,int k){
		int product;
		int j1 = Math.abs(j);
		int k1 = Math.abs(k);
		product = multPos(j1,k1);
		if(j < 0 && k > 0){
			product = -product;
		}
		else if(j > 0 && k < 0){
			product = -product;
		}
		else if(j == 0 || k == 0){
			product = 0;
		}
		return product;
	}

	/**
	 * expt the value of an integer multiplied by itself a number of times	
	 * @param n an integer
	 * @param k a non-negative integer
	 * @return exponentiation, the value of n to the power k
	 */
	public static int expt(int n,int k){
		/*int exponentiation=1;
		if(k > 0){
			for(int i = 0;i < k;i++){
				exponentiation=exponentiation*n;
			}
		}
		return exponentiation;*/
		
		//Above is the old iteration method, blew is new recursion method 
		
		if (k==1) {					//situation about k=1, n's 1st power is n
			return n;
		}
		if (k==0) {					//situation about k=0, n's 0'th power is 1
			return 1;
		}
		return (n*expt(n,k-1));		//situation about k=n(n>1), n's k'th power is n*(n^(k-1))
		
	}

	/**
	 * isPalindrome to determine whether a word that reads the same forwards and backwards	
	 * @param s a string
	 * @return a boolean, whether or not s is a palindrome
	 */
	public static boolean isPalindrome(String s){
		/*for(int i = 0;i < s.length()/2;i++){
			if(s.charAt(i)!=s.charAt(s.length()-1-i)){return false;}
		}
		return true;*/
		
		//Above is the old iteration method, blew is new recursion method 
		
		if (s.length()<=1) {
			return true;
		}
		if (s.length()==2){
			if (s.charAt(0)==s.charAt(1)) {
				return true;
			}
			return false;
		}
		System.out.println(s);
		//System.out.println((s.charAt(1)==s.charAt(s.length()))+" "+(isPalindrome(s.substring(1,s.length()-1) ) ) );
		return (s.charAt(0)==s.charAt(s.length()-1)) && (isPalindrome(s.substring(1,s.length()-1)));
		
	}

	/**
	 * fibonacci a mathematical sequence
	 * @param n a positive integer
	 * @return a, an array that contains the first n Fibonacci numbers
	 */
	public static int[] fibonacci(int n){
		int[]a = new int[n];
		a[0] = 1;
		for(int i=1;i<n;i++){
			if(i==1){
				a[i]=1;
			}
			else{
				a[i]=a[i-1]+a[i-2];
			}
		}
		return a;
	}

	/**
	 * computeGrade calculate the final grade of this course	
	 * @param labs an array of labs' grades
	 * @param quizzes an array of quizzes' grades
	 * @param exams an array of exams' grades
	 * @param studios a boolean to determine a student get this grade whether or not
	 * @param extentions extensions' grade showed by an integer
	 * @return a String, the appropriate letter grade for the input values
	 */
	public static String computeGrade(double[] labs, double[] quizzes, double[] exams,boolean studios, int extentions){
		double exam=arrsumdouble(exams)/exams.length*0.45;              //exams take 45% weight
		double lab=arrsumdouble(labs)/labs.length*0.15;                 //labs take 15% weight
		double min=quizzes[0];
		for(int i=1;i<quizzes.length;i++){
			if(quizzes[i]<min){
				min=quizzes[i];
			}
		}
		double quiz=(arrsumdouble(quizzes)-min)/(quizzes.length-1)*0.1; //quizzes take 10% weight
		double studio=0;
		if (studios) { 
			studio=10;
		}                                                               //studios take 10% weight, so it's 10 scores
		double ex=extentions*0.2;                                       //extentions take 20% weight
		double sum=(exam+lab+quiz+studio+ex);
		if (sum>=97){ return "A+"; }
		if (sum>=93){ return "A"; }
		if (sum>=90){ return "A-"; }
		if (sum>=87){ return "B+"; }
		if (sum>=83){ return "B"; }
		if (sum>=80){ return "B-"; }
		if (sum>=77){ return "C+"; }
		if (sum>=73){ return "C"; }
		if (sum>=70){ return "C-"; }
		if (sum>=60){ return "D"; }
		return "F";
	}

	public static double arrsumdouble(double[] a){
		double sum=0;
		for (int i=0;i<a.length;i++){
			sum+=a[i];
		}
		return sum;
	}
}