package lab8;

/*	TA: VictorL
* 	Grade: 100/100
* 	GJ!
*/

import java.util.Iterator;

import java.util.LinkedList;

/**
 * 
 * @author Shaoyang Zhou 440747 
 * @author Runbo Yang 439275
 *
 */

public class Polynomial {

	final private LinkedList<Double> list;

	/**
	 * Constructs a Polynomial with no terms yet.
	 */
	public Polynomial() {
		//
		// Set the instance variable (list) to be a new linked list of Double type
		//
		list = new LinkedList<Double>();   // FIXME
	}
	
	/**
	 * Make a string present this polynomial
	 */
	public String toString() {
		String s="";
		for (int i=this.list.size()-1;i>=0;i--){
			if (this.list.get(i)>0){
				if (i != (list.size()-1)){
					s=s+"+";
				}
				s=s+this.list.get(i);
				if (i>0) {
					s=s+"x^"+i;
				}
			}else if(this.list.get(i)<0){
				s=s+this.list.get(i);
				if (i>0) {
					s=s+"x^"+i;
				}
			}
		}
		return s; // FIXME
	}
	
	/**
	 * get the size
	 * @return the size of this polynomial
	 */
	public int size(){
		return this.list.size();
	}
	
	/**
	 * get the number in exact position in this polynomial
	 * @param index is the index that user want to get from this polynomial
	 * @return the exact number that is in the index of the list for this polynomial
	 */
	public double getPara(int index){
		return this.list.get(index);
	}
	
	/**
	 * add term to this polynomial
	 * @param coeff is the coeff for the next degree of this polynomial
	 * @return the whole polynomial that we current have so that we can call a lot of this operation in one line code
	 */
	public Polynomial addTerm(double coeff) {
		this.list.add(coeff);
		return this;  // required by lab spec
	}
	
	/**
	 * practice a specific number to the polynomial
	 * @param x is the number that we try to evaluate to the current polynomial
	 * @return the result that we practice x into the current polynomial
	 */
	public double evaluate(double x) {

		//we should give the polynomial that has no number in it or we will get indexOutBound error
		if (this.list.size()==0) {			
			return 0;
		}
		return calculate(x,0);  // FIXME
	}
	
	/**
	 * a assistant function that practice the x evaluate to the current polynomial using recursion
	 * @param x is the number that we try to evaluate to the current polynomial
	 * @param index the degree we are current calculating
	 * @return the result that we practice x into the current polynomial
	 */
	private double calculate(double x,int index){
		if (index>=this.list.size()-1){
			return this.list.get(index);
		}else{
			return (calculate(x,index+1)*x+this.list.get(index));
		}
	}
	
	/**
	 * figure out the derivative polynomial for this polynomial
	 * @return the derivative polynomial for this polynomial
	 */
	public Polynomial derivative() {
		Polynomial afterD=new Polynomial();
		
		//we should give the polynomial that has no number in it or we will get indexOutBound error
		if (this.list.size()==0){
			return afterD;
		}
		
		double[] para=new double[this.list.size()-1];
		for (int i=0;i<this.list.size()-1;i++){
			para[i]=this.list.get(i+1)*(i+1);
			afterD.addTerm(para[i]);
		}
		
		return afterD;   // FIXME
	}
	
	/**
	 * give out a new polynomial after add another to current one
	 * @param another another polynomial
	 * @return the new polynomial
	 */
	public Polynomial sum(Polynomial another) {
		Polynomial afterS=new Polynomial();
		int sizeThis=this.list.size();
		int sizeA=another.size();
		int i=0;
		while (i<(max(sizeA,sizeThis))){
			double t1=0,t2=0;
			if (i<sizeThis) {
				t1=this.list.get(i);
			}
			if (i<sizeA){
				t2=another.getPara(i);
			}
			afterS.addTerm(t1+t2);
			i++;
		}
		return afterS;   // FIXME
	}
	
	/**
	 * to give the bigger one for two integer
	 * @param a
	 * @param b
	 * @return the bigger one
	 */
	public static int max(int a, int b){
		if (a>b) {
			return a;
		}
		return b;
	}

	/**
	 * This is the "equals" method that is called by
	 *    assertEquals(...) from your JUnit test code.
	 *    It must be prepared to compare this Polynomial
	 *    with any other kind of Object (obj).  Eclipse
	 *    automatically generated the code for this method,
	 *    after I told it to use the contained list as the basis
	 *    of equality testing.  I have annotated the code to show
	 *    what is going on.
	 */

	public boolean equals(Object obj) {
		// If the two objects are exactly the same object,
		//    we are required to return true.  The == operator
		//    tests whether they are exactly the same object.
		if (this == obj)
			return true;
		// "this" object cannot be null (or we would have
		//    experienced a null-pointer exception by now), but
		//    obj can be null.  We are required to say the two
		//    objects are not the same if obj is null.
		if (obj == null)
			return false;

		//  The two objects must be Polynomials (or better) to
		//     allow meaningful comparison.
		if (!(obj instanceof Polynomial))
			return false;

		// View the obj reference now as the Polynomial we know
		//   it to be.  This works even if obj is a BetterPolynomial.
		Polynomial other = (Polynomial) obj;

		//
		// If we get here, we have two Polynomial objects,
		//   this and other,
		//   and neither is null.  Eclipse generated code
		//   to make sure that the Polynomial's list references
		//   are non-null, but we can prove they are not null
		//   because the constructor sets them to some object.
		//   I cleaned up that code to make this read better.


		// A LinkedList object is programmed to compare itself
		//   against any other LinkedList object by checking
		//   that the elements in each list agree.

		return this.list.equals(other.list);
	}

}
