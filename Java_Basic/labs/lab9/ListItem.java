package lab9;
/**
 * 
 * @author Shaoyang Zhou 440747
 * @author Runbo Yang 439275
 *
 */

/* TA: MichaelM
 * Grade: 98/100
 * No in-line comments to explain the more complicated methods -2
 * 
 */

public class ListItem {
	//
	// Important:  Do NOT make these instance variables private
	// Our testing needs to be able to access them, so leave their
	//   declarations as you see them below.
	//
	final int number;
	ListItem next;

	/**
	 * Creates a single list item.
	 * @param number the value to be held in the item
	 * @param next a reference to the next item in the list
	 */
	ListItem(int number, ListItem next) {
		this.number = number;
		this.next   = next;
	}
	
	/**
	 * Creates a single list item.
	 * @param number the value to be held in the item
	 */
	ListItem(int number) {
		this.number = number;
		//this.next   = next;
	}
	

	/**
	 * Return a copy of this list using recursion.  No
	 * credit if you use any iteration!  All existing lists should remain
	 * intact -- this method must not mutate anything.
	 * @return
	 */
	public ListItem duplicate() {
		if (this.next==null) {
			return new ListItem(this.number,null);
		}else{
			return new ListItem(this.number,this.next.duplicate());
		}// FIXME
	}

	/**
	 * Recursively compute the number of elements in the list. No
	 * credit if you use any iteration!  All existing lists should remain
	 * intact.
	 * @return
	 */
	public int length() {
		if (this.next==null){
			return 1;
		}
		return (1+this.next.length()); 
		// FIXME
	}

	/**
	 * Return a new list, like duplicate(), but every element
	 * appears n times instead of once.  See the web page for details.
	 * You must do this method iteratively.  No credit
	 * if you use any recursion!
	 * @param n a positive (never 0) number specifying how many times to copy each element
	 * @return
	 */

	public ListItem stretch(int n) {
		ListItem head=subStretch(n,this);
		ListItem temp=head.findTail();
		ListItem tempC=this.next;
		while (tempC != null){
			temp.next=subStretch(n,tempC);
			temp=temp.findTail();
			tempC=tempC.next;
		}
		return head;
	}
	
	/**
	 * sub method of stretch(int n)
	 * return the result of stretch the single item to n times
	 * @param n a positive (never 0) number specifying how many times to copy each element
	 * @param ls the item need to stretch
	 * @return the list that contains n same items which is cloned by para ls
	 */
	public static ListItem subStretch(int n,ListItem ls){
		ListItem temp=new ListItem(ls.number,null);
		ListItem tempC=temp;
		for (int i=1;i<n;i++) {
			tempC.next=new ListItem(ls.number,null);
			tempC=tempC.next;
		}
		return temp;
	}
	
	/**
	 * sub method of stretch(int n)
	 * find the tail of one list
	 * @return the tail node
	 */
	public ListItem findTail(){
		ListItem temp=this;
		while (temp.next!=null){
			temp=temp.next;
		}
		return temp;
	}

	/**
	 * Return the first ListItem, looking from "this" forward,
	 * that contains the specified number.  No lists should be
	 * modified as a result of this call.  You may do this recursively
	 * or iteratively, as you like.
	 * @param n
	 * @return
	 */

	public ListItem find(int n) {
		if (this.next==null){
			if (this.number!=n){
				return null;
			}else{
				return this;
			}
		}
		if (this.number==n){
			return this;
		}else{
			return this.next.find(n);
		}
	}

	/**
	 * Return the maximum number contained in the list
	 * from this point forward.  No lists should be modified
	 * as a result of this call.  You may do this method recursively
	 * or iteratively,as you like.
	 * @return
	 */

	public int max() {
		ListItem m=this;
		int maxx=this.number;
		while (m!=null){
			if (m.number>maxx){
				maxx=m.number;
			}
			//System.out.print(this.number+"/");
			m=m.next;
		}
		return maxx; // FIXME
	}

	/**
	 * Returns a copy of the list beginning at ls, but containing
	 * only elements that are even.
	 * @param ls
	 * @return
	 */
	public static ListItem evenElements(ListItem ls) {
		if (ls==null){
			return null;
		}
		if (ls.number%2==0){
			return new ListItem(ls.number,evenElements(ls.next));
		}else{
			return evenElements(ls.next);
		}
	}	


	/**
	 * Returns a string representation of the values reachable from
	 * this list item.  Values appear in the same order as the occur in
	 * the linked structure.  Leave this method alone so our testing will work
	 * properly.
	 */
	public String toString() {
		if (next == null)
			return ("" + number);
		else
			return (number + " " + next); // calls next.toString() implicitly
	}

}
