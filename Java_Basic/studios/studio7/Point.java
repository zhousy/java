package studio7;

public class Point {
	
	private final double deltaX;
	private final double deltaY;
	
	/**
	 * 
	 * @param initialFunds initial deltaX and deltaY with input x and y
	 */
	
	public Point(double x,double y){
		this.deltaX=x;
		this.deltaY=y;
	}
	
	public double getDeltaY(){
		return this.deltaY;
	}
	
	public double getDeltaX(){
		return this.deltaX;
	}
	
	public Vector plus(Vector v){
		return new Vector(this.deltaX+v.getDeltaX(),this.deltaY+v.getDeltaY());
	}
	
	public Vector minus(Vector v){
		return new Vector(this.deltaX-v.getDeltaX(),this.deltaY-v.getDeltaY());
	}
	
	public Vector scale(double times){
		return new Vector(this.deltaX*times,this.deltaY*times);
	}
	
	public Vector deflectX(){
		return new Vector(-this.deltaX,this.deltaY);
	}
	
	public Vector deflectY(){
		return new Vector(this.deltaX,-this.deltaY);
	}
	
	public double magnitude(){
		return Math.sqrt(this.deltaX*this.deltaX+this.deltaY*this.deltaY);
	}
	
	public Vector rescale(double times){
		double frc=Math.sqrt(this.deltaX*this.deltaX+this.deltaY*this.deltaY);
		if (frc==0){
			return new Vector(times,0);
		}
		return new Vector(this.deltaX*times/frc,this.deltaY*times/frc);
	}

}
