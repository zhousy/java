package studio9;

public class ListNode {
	int value;
	public ListNode next;
	public ListNode prev;
	
	public ListNode(){
		
	}
	
	public ListNode(int value){
		this.value = value;
	}
	
	public ListNode(int value, ListNode next, ListNode prev){
		this.value = value;
		this.next = next;
		this.prev=prev;
	}
}
