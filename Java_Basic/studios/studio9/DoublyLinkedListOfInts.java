package studio9;

import studio9.ListNode;

public class DoublyLinkedListOfInts implements List<Integer> {
	
	final private ListNode head;
	final private ListNode tail;
	private int size;
	
	public DoublyLinkedListOfInts(){
		this.head=new ListNode();
		this.tail=new ListNode();
		this.head.next=this.tail;
		this.tail.prev=this.head;
		this.size=0;
	}

	@Override
	public void addFirst(Integer x) {
		// TODO Auto-generated method stub
		ListNode p = new ListNode(x);
		p.next = this.head.next;
		this.head.next.prev=p;
		this.head.next = p;
		p.prev=this.head;
		this.size++;
	}

	@Override
	public void addLast(Integer x) {
		// TODO Auto-generated method stub
		ListNode p = new ListNode(x);
		this.tail.prev.next=p;
		p.prev=this.tail.prev;
		this.tail.prev=p;
		p.next=this.tail;
		this.size++;
	}

	@Override
	public boolean remove(Integer x) {
		ListNode p = this.head.next;
		while (p != this.tail) {
			if (p.next.value == x) {
				p.next.next.prev=p;
				p.next = p.next.next;
				this.size--;
				return true;
			}else{
				p=p.next;
			}
		}
		return false;
	}
	
	@Override
	public int indexOf(int i) {
		ListNode p = head.next;
		int current = 0;
		while (p != null) {
			if (i == current) {
				return current;
			} else {
				current++;
				p = p.next;
			}
		}
		//throw new IndexOutOfBoundsException(String.valueOf(i));
		return -1;
		
	}

	@Override
	public Integer get(int i) throws IndexOutOfBoundsException {
		// TODO Auto-generated method stub
		ListNode p = head.next;
		int current = 0;
		while (p != null) {
			if (i == current) {
				return p.value;
			} else {
				current++;
				p = p.next;
			}
		}
		throw new IndexOutOfBoundsException(String.valueOf(i));
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return this.size;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if (size==0) {
		return true;}
		return false;
	}

}
