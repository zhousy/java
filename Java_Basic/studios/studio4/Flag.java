package studio4;

import sedgewick.StdAudio;
import sedgewick.StdDraw;
import sedgewick.StdIn;

import java.awt.Color;

import cse131.ArgsProcessor;

public class Flag {
	
	public static void main(String[] args) {
		ArgsProcessor.useStdInput("music");
		double pp=0.5;
		double ppy=0.5;
		double step=0.01;
		double stepy=0.011;
		int loops=1;
		int shwt=25;
		while (true) {
			StdDraw.clear();
			loops++;
			if (loops==3) {
				loops=1;
			}
			if (loops==1){
				putstrap(0.03,0);
			}
			else{
				putstrap(0.03,0.03);
			}
			StdDraw.setPenColor(Color.red);
			StdDraw.filledRectangle(0.5, 0.5, 0.25+0.125, 0.25);
			double bsp=0.125;
			double bspy=0.75;
			double c=0.025;
			putstar(0.03,bsp+5*c,bspy-5*c);
			putstar(0.015,bsp+10*c,bspy-2*c);
			putstar(0.015,bsp+12*c,bspy-4*c);
			putstar(0.015,bsp+12*c,bspy-7*c);
			putstar(0.015,bsp+10*c,bspy-9*c);
			pp = pp + step; // move 
			if ( pp >= 0.875) {
				step= -1* step;
			}
			else if ( pp <= 0.125) {
				step = -1*step;
			}
			ppy = ppy + stepy; // move 
			if ( ppy >= 0.75) {
				stepy= -1* stepy;
			}
			else if ( ppy <= 0.25) {
				stepy = -1*stepy;
			}
			StdDraw.setPenColor(Color.black);
			StdDraw.text(pp, ppy,"China!");
			StdDraw.show(shwt);
			if (!StdIn.isEmpty()){
				int p = StdIn.readInt();
				double d = StdIn.readDouble();
				double hz = 440 * Math.pow(2, p / 12.0);
				int N = (int) (StdAudio.SAMPLE_RATE * d);
				double[] a = new double[N+1];
				for (int i = 0; i <= N; i++) {
					a[i] = Math.sin(2 * Math.PI * i * hz / StdAudio.SAMPLE_RATE);
				}
				StdAudio.play(a);
			}
			else{
				shwt=250;
			}
		}



	
		
	}
	
	public static void putstar(double a,double cx,double cy){
		StdDraw.setPenColor(Color.yellow);
		double fct=Math.PI/180;
		double s=(a/2)/Math.cos(72*fct);
		double[] x={(cx-(a/2)-s),(cx-(a/2)),(cx),(cx+(a/2)),(cx+(a/2)+s),
				(cx+a/2+a*Math.sin(18*fct)),(cx+s*Math.sin(72*fct)),
				(cx),(cx-s*Math.sin(54*fct)),(cx-a/2-a*Math.sin(18*fct))};
		double[] y={(cy),(cy),(cy+s*Math.sin(72*fct)),(cy),(cy),(cy-a*Math.cos(18*fct)),
				(cy-(s+a)*Math.sin(72*fct)),(cy+s*Math.cos(54*fct)-(s+a)*Math.sin(72*fct)),(cy-(s+a)*Math.sin(72*fct)),
				(cy-a*Math.cos(18*fct))};
		//StdDraw.polygon(x, y);
		StdDraw.filledPolygon(x, y);
		}
	public static void putstrap(double wi,double fp){
		StdDraw.setPenColor(Color.BLUE);
		for (int i=0;i<=(1/wi+10);i+=2){
			double[] xx={fp+wi*i,fp+wi*(i+1),fp+1+wi*(i+1),fp+1+wi*i};
			int j=-1*i-1;
			double[] xx2={fp+wi*j,fp+wi*(j-1),fp+1+wi*(j-1),fp+1+wi*j};
			double[] yy={0,0,1,1};
			StdDraw.filledPolygon(xx,yy);
			StdDraw.filledPolygon(xx2,yy);
			
		}
	}
	
		
	}