package studio3;

import java.text.DecimalFormat;

import cse131.ArgsProcessor;

public class Dice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArgsProcessor ap = new ArgsProcessor(args);
		int d=ap.nextInt("Input the amount of dice(s):");
		while (d==0){
			d=ap.nextInt("Amount of dice(s) cannot be 0:");
		}
		int t=ap.nextInt("Input the time(s) ot simulation:");
		while (t==0){
			t=ap.nextInt("At least you should simulate 1 time:");
		}
		double rates=0;
		int ratesoc=0;
		int sumt;
		int sum[]=new int[t];
		int sumoc[]=new int[6*d+1];
		int tabl[][]=new int[t][d];
		DecimalFormat df2  = new DecimalFormat("###.00");

		for(int i=0;i<t;i++){
			sumt=0;
			boolean flag=true;
			for(int j=0;j<d;j++){
				tabl[i][j]=makedicenum();
				if ((j>0)&&(flag)){
					flag=((tabl[i][j])==(tabl[i][j-1]));
				}
				sumt=sumt+tabl[i][j];
			}
			if (flag){
				ratesoc++;
			}
			
			sum[i]=sumt;
			sumoc[sumt]++;
		}
		rates=ratesoc/(t*1.0);
		
		//output the simulate result
		givealine(d);
		System.out.println("Result Table	");
		givealinein(d);
		for (int j=0;j<d;j++){
			System.out.print((j+1)+"	");
		}
		System.out.println();
		givealinein(d);
		for(int i=0;i<t;i++){
			for(int j=0;j<d-1;j++){
				System.out.print(tabl[i][j]+"	");
			}
			System.out.println(tabl[i][d-1]+"	Sum="+sum[i]);
		}
		givealine(d);
		System.out.println();
		
		
		System.out.println("=======================");
		System.out.println("The sum occurance talbe");
		System.out.println("-----------------------");
		System.out.println("num	occurance");
		System.out.println("-----------------------");
		for(int i=d;i<(6*d+1);i++){
			if (sumoc[i]>0){
				System.out.println(i+"	"+sumoc[i]);
			}
		}
		System.out.println("=======================");
		System.out.println();
		
		
		System.out.println("=================");
		System.out.println("The rates of same");
		System.out.println("-----------------");
		System.out.println(df2.format(rates*100)+"%");
		System.out.println("=================");


	}
	
	public static int makedicenum(){
		int re=0;
		while ((re>6)||(re<1)){
			double r=Math.random();
			re=(int)(r*10);
		}
		return(re);
	}
	public static void givealine(int l){
		System.out.print("=");
		if (l<3) {
			l=3;
		}
		for (int k=0;k<(l-1);k++){
			System.out.print("========");
		}
		System.out.println();
	}
	public static void givealinein(int l){
		System.out.print("-");
		if (l<3) {
			l=3;
		}
		for (int k=0;k<(l-1);k++){
			System.out.print("--------");
		}
		System.out.println();
	}
}
