package studio8;

import java.util.HashSet;
import java.util.LinkedList;

public class Date {
	public int month;
	public int day;
	public int year;
	public boolean holiday;
	
	public Date(int month,int day,int year,boolean holiday){
		this.month=month;
		this.year=year;
		this.day=day;
		this.holiday=holiday;
	}
	
	  public boolean amWorking(){
		  if ((day%2)==0){
			  return true;
		  }
		  return false;
	  }
	public Date(int month,int day,int year){
		this.month=month;
		this.year=year;
		this.day=day;
		this.holiday=false;
	}
	
	public String toString(){
		String holidayOrNot="is not";
		if (this.holiday){
			holidayOrNot="is";
		}
		return (this.year+"-"+this.month+"-"+this.day+ " which "+ holidayOrNot+ " a holiday");
	}
	
	boolean isEarlierThan(Date other){
		if (this.year>other.year) {
			return false;}else{
			if (this.month>other.month){
				return false;}else{
				if (this.day>=other.day){
					return false;
				}
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		Date d1=new Date(9,30,1991,true);
		Date d2=new Date(9,30,1991,false);
		System.out.println(d1.isEarlierThan(d2));
		System.out.println(d1.amWorking());
		System.out.println(d1.equals(d2));
		LinkedList<Date> list = new LinkedList<Date>();
		list.add(d1);
		list.add(d2);
		list.add(d1);
		System.out.println(list);
		HashSet<Date> set = new HashSet<Date>();
		set.add(d1);
		set.add(d2);
		set.add(d1);
		System.out.println(set);
		
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + day;
		result = prime * result + (holiday ? 1231 : 1237);
		result = prime * result + month;
		result = prime * result + year;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Date other = (Date) obj;
		if (day != other.day)
			return false;
		if (holiday != other.holiday)
			return false;
		if (month != other.month)
			return false;
		if (year != other.year)
			return false;
		return true;
	}

}
