package studio8;

import java.util.LinkedList;

public class Time implements Working{
	public int hour;
	public int minute;
	public int second;
	private boolean format;
	
	public boolean amWorking(){
		if ((hour>=9)&&(hour<=17)) {
			return true;
		}
		return false;
	}
	
	public Time (int hour,int minute,int second,boolean format){
		this.hour=hour;
		this.minute=minute;
		this.second=second;
		this.format=format;
	}
	public Time (int hour,int minute,int second){
		this.hour=hour;
		this.minute=minute;
		this.second=second;
		this.format=false;
	}
	
	public String toString(){
		if (!this.format){
			return (this.hour+":"+this.minute+":"+this.second);
		}else{
			if (((this.hour-1) / 12)>=1){
				return ((this.hour%12)+":"+this.minute+":"+this.second+" pm");
			}else{
				return ((this.hour%12)+":"+this.minute+":"+this.second+" am");
			}
		}
	}
	
	boolean isEarlierThan(Time other){
		if (this.hour>other.hour) {
			return false;}else{
			if (this.minute>other.minute){
				return false;}else{
				if (this.second>=other.minute){
					return false;
				}
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		Time t1=new Time(23,14,07,true);
		Time t2=new Time(11,14,07,true);
		System.out.println(t1.toString());
		System.out.println(t2.toString());
		System.out.println(t1.equals(t2));
		LinkedList<Time> list=new LinkedList<Time>();
		list.add(t1);
		list.add(t2);
		System.out.println(list);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (format ? 1231 : 1237);
		result = prime * result + hour;
		result = prime * result + minute;
		result = prime * result + second;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Time other = (Time) obj;
		if (format != other.format)
			return false;
		if (hour != other.hour)
			return false;
		if (minute != other.minute)
			return false;
		if (second != other.second)
			return false;
		return true;
	}

}
