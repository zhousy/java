package studio8;

public class Appointment {
	
	private Date date;
	private Time time;
	String withWho;
	String doWhat;
	String inWhere;
	
	public Appointment(Date date,Time time,String withWho,String doWhat,String inWhere){
		this.date=date;
		this.time=time;
		this.withWho=withWho;
		this.doWhat=doWhat;
		this.inWhere=inWhere;
	}
	
	public String toString(){
		return (this.date.toString()+" "+this.time.toString()+" with "+this.withWho+" "+this.inWhere+" "+this.doWhat);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Time t1=new Time(23,14,07,true);
		Date d1=new Date(9,30,1991,true);
		Appointment ap1=new Appointment(d1,t1,"DiMou","having sex","at home");
		System.out.println(ap1);

	}
	
	public Date getDate(){
		return this.date;
	}
	public Time getTime(){
		return this.time;
	}
	
	public boolean hasSameTime(Appointment ap){
		if ((this.date==ap.getDate())&&(this.time==ap.getTime())) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Appointment other = (Appointment) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}

}
