package studio8;

import java.util.HashSet;

public class Calendar {
	HashSet<Appointment> set;
	
	public Calendar(){
		this.set= new HashSet<Appointment>();
	}
	
	public boolean add(Appointment ap){
		for (Appointment api:this.set){
			if (api.hasSameTime(ap)){
				return false;
			}
		}
		return(this.set.add(ap));
	}
	
	public boolean delete(Appointment ap){
		return(this.set.remove(ap));
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
