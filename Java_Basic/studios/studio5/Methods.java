package studio5;

public class Methods {

	public static int sum(int x, int y) {
		return x+y;
	}
	

	public static int mpy(int x, int y) {
		return(x*y);
	}
	
	public static double avg3(int x,int y,int z){
		return ((x+y+z)/3.0);
	}
	
	public static double sumArray(double a[]){
		int l=a.length;
		double sum=0;
		for (int i=0;i<l;i++){
			sum=sum+a[i];
		}
		return sum;
	}
	
	public static double average(double a[]){
		return(sumArray(a)/(a.length*1.0));
	}
	
	public static String pig(String s){
		return((s.substring(1)+s.substring(0,1)+"ay"));
	}
	
	public static int add1(int a){
		return(a+1);
	}
	


}
