package watermelons;

import java.util.Arrays;

public class Watermelons {
	//static int[] local=new int[10];
	/**
	 * Computes the sum of each distinct pair of entries in the incoming array.
	 * A given pair of entries has its sum computed only once.  So if you
	 * compute the sum for entries 2 and 4, and that sum appears in your answer
	 * array, then you do not also compute the sum of entries 4 and 2.
	 * Depending on the input, the same sum value may appear multiple times in the result.
	 * For example, if all of the incoming values are 0, then so are all of the returned values.
	 * @param nums an array of integers, possibly containing duplicates
	 * @return an array containing the sums of pairs as described above
	 */
	public static int[] allPairSums(int[] nums) {
		if (nums.length<=1){
			return new int[0];
		}
		int[] ans = new int[jc(nums.length)/2];  // FIXME compute ans as the result you want
		int k=0;
		for (int i=0;i<nums.length;i++){
			for (int j=0;j<i;j++){
				ans[k]=nums[i]+nums[j];
				k++;
			}
		}
		//for (int i=0;i<ans.length;i++){
		//	local[i]=ans[i];
		//}
		return ans;
	}
	
	/**
	 * The method below must COMPUTE and return a solution to the puzzle posted
	 * on the page that describes this extension.  You must compute the solution by trying
	 * lots of possibilities, and finding the one that yields the right answer.
	 * You can run the unit test testPuzzleSolution to see if you are right.
	 * @return
	 */
	public static int jc(int n){
		return n*(n-1);
	}
	public static int[] getSolution() {
		int[] local=new int[WatermelonsTest.expected.length];
		int[] re=new int[5];
		for (int i=0;i<WatermelonsTest.expected.length;i++){
			local[i]=WatermelonsTest.expected[i];
		}
		Arrays.sort(local);
		
		for (int i1=0;i1<=31;i1++){
			for (int i2=0;i2<=31;i2++){
				for (int i3=0;i3<=31;i3++){
					for (int i4=0;i4<=31;i4++){
						for (int i5=0;i5<=31;i5++){
							int[] ne=new int[10];
							ne=allPairSums(new int[]{i1,i2,i3,i4,i5});
							Arrays.sort(ne);
							//System.out.println(i1+" "+i2+i3+i4+i5);
							if (Arrays.equals(ne,local)){
								re[0]=i1;re[1]=i2;re[2]=i3;re[3]=i4;re[4]=i5;
								break;
							}
						}
					}
				}
			}
		}
		return re;
	}

}
