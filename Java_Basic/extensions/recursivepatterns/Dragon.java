package recursivepatterns;

import sedgewick.StdDraw;

public class Dragon {

	public static void main(String[] args) {
		boolean[] turns = getArray(8);
		//for (int i=0;i<turns.length;i++){System.out.println(turns[i]);}
		drawTurns(turns, 8);//FIXME: Replace with a call to your method
	}
	
	public static boolean[] getArray(int n){
		if (n==1) {
			//int nw=(int)(Math.random()*2);
			boolean[] re=new boolean[1];
			//if (nw%2==0){
			re[0]=true;//}else{re[0]=false;}
			return re;
		}
		boolean[] left=getArray(n-1);
		boolean[] right=new boolean[left.length];
		for (int i=0;i<left.length;i++){
			right[i]=!(left[left.length-i-1]);
		}
		//for (int i=0;i<left.length;i++){System.out.println(left[i]);}
		//for (int i=0;i<right.length;i++){System.out.println(right[i]);}
		boolean[] res=new boolean[left.length*2+1];
		int j=0;
		for (int i=0;i<left.length;i++){
			res[j]=left[i];
			j++;
		}
		res[j]=true;
		j++;
		//for (int i=0;i<res.length;i++){System.out.println(res[i]);}
		for (int i=0;i<right.length;i++){
			res[j]=right[i];
			j++;
		}
		return res;
		
	}

	public static void drawTurns(boolean[] turns, int depth) {
		double x0 = 0.5, y0 = 0.5, angle = 0;

		double length = 0.35 / Math.pow(2, depth / 2);

		for (int i = 0; i < turns.length; i++) {

			double x1 = x0 + length * Math.cos(angle);
			double y1 = y0 + length * Math.sin(angle);

			StdDraw.line(x0, y0, x1, y1);
			x0 = x1;
			y0 = y1;
			if (turns[i])
				angle += Math.PI / 2;
			else
				angle -= Math.PI / 2;
			StdDraw.pause((int)(length*1000));

		}

		double x1 = x0 + length * Math.cos(angle);
		double y1 = y0 + length * Math.sin(angle);

		StdDraw.line(x0, y0, x1, y1);

	}

}
