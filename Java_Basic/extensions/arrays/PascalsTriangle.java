package arrays;
import cse131.ArgsProcessor;

public class PascalsTriangle {
	
	public static void main(String[] args) {
		ArgsProcessor ap=new ArgsProcessor(args);
		int N=ap.nextInt("N:");
		int[][] result=new int[N+2][N+2];
		for (int k=N-1;k>0;k--){
			System.out.print("	");
		}
		result[1][1]=1;
		System.out.println(result[1][1]);
		for (int i=2;i<N+1;i++){
			for (int k=N-i;k>0;k--){
				System.out.print("	");
			}
			for (int j=1;j<=i;j++){
				result[i][j]=result[i-1][j-1]+result[i-1][j];
				System.out.print(result[i][j]+"		");
			}
			System.out.println();
		}
		
	}

}
