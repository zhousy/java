package arrays;
import cse131.ArgsProcessor;

public class Birthday {
	
	public static void main(String[] args) {
		
		ArgsProcessor ap = new ArgsProcessor(args);
		int N=ap.nextInt("N:");
		
		int[] day=new int[32];
		int[] mon=new int[13];
		int[][] per=new int[N][2];
		
		for (int i=0;i<N;i++){
			int d=(int)(Math.random()*31+1);
			int m=(int)(Math.random()*12+1);
			day[d]++;
			mon[m]++;
			per[i][0]=m;
			per[i][1]=d;
		}
		
		double sumd=0;
		double summ=0;
		
		for (int i=1;i<13;i++){
			System.out.print(mon[i]/(N*1.0)+"	");
			summ+=mon[i]/(N*1.0);
		}
		System.out.println();
		System.out.println(summ/12);
		
		for (int i=1;i<32;i++){
			System.out.print(day[i]/(N*1.0)+"	");
			sumd+=day[i]/(N*1.0);
		}
		System.out.println();
		System.out.println(sumd/31);
		
		int sum=0;
		for (int i=1;i<N;i++){
			for (int j=0;j<i;j++){
				if ((per[i][0]==per[j][0]) && (per[i][1]==per[j][1])) {
					sum++;
				}
			}
		}
		
		System.out.println(sum/(N*1.0));
		
	}

}
